<!DOCTYPE HTML>
<html>
<head></head>
<body>

<?php
session_start();

if($_SESSION['login'] == true){
	echo 'hi dere ';
}else{
	echo 'nop';
	header("Location: auth/login.php");
}

if(isset($_SESSION['logged_name'])){
	echo $name = $_SESSION['logged_name']; echo "</br>";
}

$post_title = $post_text = "";
$post_title_err = $post_text_err = "";
date_default_timezone_set('Asia/Tbilisi');
$date_created =  date('Y-m-d H:i:s');
$date_updated =  date('Y-m-d H:i:s');

if ($_SERVER["REQUEST_METHOD"] == "POST"){
	require_once 'checks/check_create_post_form.php';

	// here i will save the post to db, if the form is submitted, without errors
	if(!$post_title_err && !$post_text_err){
		require_once 'db_connect.php';
		$sql = "SELECT id, name FROM users WHERE name='$name'";
		$query = mysqli_query($conn, $sql);
		$row = mysqli_fetch_assoc($query);
		$user_id =  $row['id'];
		$user_name =  $row['name'];

		if(!empty($_FILES["fileToUpload"]["name"])){
			$file_name = $_FILES["fileToUpload"]["name"];
			$file_tmp_name = $_FILES["fileToUpload"]["tmp_name"];
			$file_size = $_FILES["fileToUpload"]["size"];

			$target_dir = "uploads/";
			$image_path = $target_dir . basename($file_name);
			$uploadOk = 1;
			$imageFileType = strtolower(pathinfo($image_path,PATHINFO_EXTENSION));

			if ($_SERVER["REQUEST_METHOD"] == "POST"){
				// Check if image file is a actual image or fake image
				$check = getimagesize($file_tmp_name);
				if($check !== false) {
					echo "File is an image - " . $check["mime"] . ".";
					$uploadOk = 1;
					$sql = "INSERT IGNORE INTO posts (
					post_title, 
					post_text,
					user_id,
					user_name,
					image_path, 
					date_created, 
					date_updated
					)
					VALUES (
					'$post_title', 
					'$post_text',
					'$user_id',
					'$user_name',
					'$image_path', 
					'$date_created', 
					'$date_updated'
					)";
					if (mysqli_query($conn, $sql)) {
						if(mysqli_affected_rows($conn) == 0){
							echo ' post like that already exists  ';
						}else{
							echo ' post created successfully ';
						}
					} else {
						echo "Error: " . $sql . "<br>" . mysqli_error($conn);
					}
				} else {
					echo "File is not an image.";
					$uploadOk = 0;
				}
			}else{
				echo ' form is empty ';
			}
			require_once "checks/check_file_upload.php";
		}else{
			echo ' select image plz ';
		}
		mysqli_close($conn);
	}
}

?>

<br>
<a href="auth/logout.php">Logout</a>

<h3>Create your post here</h3>
<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>"
	  enctype="multipart/form-data">
	Post Title:
	<input type="text" name="post_title" value="<?php echo $post_title;?>">
	<span class="error">* <?php echo $post_title_err;?></span>
	<br><br>

	Post Text:
	<textarea name="post_text" rows="5" cols="40"><?php echo $post_text;?></textarea>
	<span class="error">* <?php echo $post_text_err;?></span>
	<br><br>

	Select image to upload:
	<input type="file" name="fileToUpload" id="fileToUpload">
	<br><br>

	<input type="submit" name="submit" value="Submit">
	<br><br>
</form>

<br><br>
<a href="index.php">Index</a><br><br>

</body>
</html>