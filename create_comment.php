<?php
session_start();
require_once 'db_connect.php';

$comment = "";
$comment_err = "";
date_default_timezone_set('Asia/Tbilisi');
$comment_date_created =  date('Y-m-d H:i:s');
$comment_date_updated =  date('Y-m-d H:i:s');

// checks data i recieve from login.php
if(!empty($_SESSION['logged_name'])){
	$user_logged = $_SESSION['logged_name'];
	echo $user_logged;
}else{
	echo " session logged_name is empty ";
}
if(!empty($_SESSION['user_logged_id'])){
	$user_id = $_SESSION['user_logged_id'];
	echo $user_id;
}else{
	echo " session user_logged_id is empty ";
}


if($_SESSION['login'] == true){
	if(isset($_GET['id'])){
		$post_id = (int)$_GET['id'];
		if($_SERVER["REQUEST_METHOD"] == "POST"){
			require_once "checks/check_create_comment.php";
			if(!$comment_err){
				$query = "SELECT 
						id, 
						image_path, 
						post_title, 
						post_text, 
						user_name, 
						date_created, 
						date_updated
					FROM posts WHERE id='$post_id'";
				$result = mysqli_query($conn, $query);
				$list = '';
				while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
					$id = $row["id"];
					$image_path = $row["image_path"];
					$post_title = $row["post_title"];
					$post_text = $row["post_text"];
					$user_name = $row["user_name"];
					$date_created = $row["date_created"];
					$date_updated = $row["date_updated"];
				}

				$query = "INSERT INTO comments(
						comment, 
						post_id, 
						user_id,  
						user_name, 
						date_created, 
						date_updated
						)
						VALUES (
						'$comment', 
						'$post_id',
						'$user_id',
						'$user_logged', 
						'$comment_date_created', 
						'$comment_date_updated'
						)";
				if (mysqli_query($conn, $query)) {
					echo " comment added successfully ";
				}
				if (!$query) {
					die('Invalid query: ' . mysqli_error($conn));
				}
			}
		}else{
			//echo " nothing was submitted ";
		}
	}
}else{
	header("Location: auth/login.php");
}
?>

<!DOCTYPE HTML>
<html>
<head>
	<style>
		.error {color: #FF0000;}
		a { text-decoration:; }

		body{ font-family: "Trebuchet MS", Arial, Helvetica, sans-serif; }
		div#pagination_controls{font-size:21px;}
		div#pagination_controls > a{ color:#06F; }
		div#pagination_controls > a:visited{color:#06F;}
	</style>
</head>
<body>
<br>
<a href="index.php">Index</a>

<form method="post" action="create_comment.php?id=<?php echo $post_id; ?>">
	Comment:
	<textarea name="comment" rows="5" cols="40"><?php echo $comment;?></textarea>
	<span class="error">* <?php echo $comment_err;?></span>
	<br><br>

	<input type="submit" name="submit" value="Submit">
	<br><br>
</form>

<br><br>
<a href="view_post.php?id=<?php echo $post_id; ?>">View Post</a>

</body>
</html>
