<?php
$servername = "localhost";
$username = "root";
$pass = "";
$dbname = "blog";
header('Content-Type: text/html; charset=utf-8');

// Create connection
$conn = mysqli_connect($servername, $username, $pass, $dbname);
mysqli_set_charset($conn, 'utf8');
// Check connection
if (!$conn) {
	die("Connection failed: " . mysqli_connect_error());
}