<!DOCTYPE HTML>
<html>
<head></head>
<body>

<?php
session_start();

// checks data i recieve from login.php
if(!empty($_SESSION['logged_name'])){
	$user_logged = $_SESSION['logged_name'];
	echo " user logged: ".$user_logged;
}else{
	echo " session logged_name is empty ";
}
if(!empty($_SESSION['user_logged_id'])){
	$user_id = $_SESSION['user_logged_id'];
	echo " id: ".$user_id;
}else{
	echo " session user_logged_id is empty ";
}

if($_SESSION['login'] == true){
	echo 'hi dere ';
}else{
	echo 'nop';
	header("Location: auth/login.php");
}

if(isset($_SESSION['logged_name'])){
	echo $name = $_SESSION['logged_name']; echo "</br>";
}

$post_title = $post_text = "";
$post_title_err = $post_text_err = "";
date_default_timezone_set('Asia/Tbilisi');
$date_created =  date('Y-m-d H:i:s');
$date_updated =  date('Y-m-d H:i:s');

$post_id = (int)$_GET['id'];

require_once 'db_connect.php';

$post_title_db = "";
$query = "SELECT 
						id, 
						image_path, 
						post_title, 
						post_text, 
						user_name, 
						date_created, 
						date_updated
					FROM posts WHERE id='$post_id'";
$result = mysqli_query($conn, $query);
while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)){
	$id = $row["id"];
	$image_path = $row["image_path"];
	$post_title_db = $row["post_title"];
	$post_text = $row["post_text"];
	$user_name = $row["user_name"];
//	$date_created = $row["date_created"];
//	$date_updated = $row["date_updated"];
}
if (!$query) {
	die('Invalid query: ' . mysqli_error($conn));
}


if ($_SERVER["REQUEST_METHOD"] == "POST"){
	require_once 'checks/check_create_post_form.php';

	// here i will save the post to db, if the form is submitted, without errors
	if(!$post_title_err && !$post_text_err){

		$sql = "SELECT id, name FROM users WHERE name='$name'";
		$query = mysqli_query($conn, $sql);
		$row = mysqli_fetch_assoc($query);
		$user_id =  $row['id'];
		$user_name =  $row['name'];

		$file_name = $_FILES["fileToUpload"]["name"];
		$file_tmp_name = $_FILES["fileToUpload"]["tmp_name"];
		$file_size = $_FILES["fileToUpload"]["size"];

		$query = "SELECT users.id, users.name, posts.user_id 
					FROM users
					JOIN posts ON users.id = posts.user_id
					WHERE posts.id = '$post_id'";
		$result = mysqli_query($conn, $query);
		if (mysqli_query($conn, $query)) {
			//header("Location: index.php");
			$row = mysqli_fetch_array($result, MYSQLI_ASSOC);
			$post_user_name = $row["name"];
			echo " user who created post: ".$post_user_name; echo "</br>";
		} else {
			echo "Error: " . $query . "<br>" . mysqli_error($conn);
		}

		if($user_logged == "sam" || $user_logged == $post_user_name){
			if(!empty($file_name)){
				// this will update post, when image is also selected
				$target_dir = "uploads/";
				$image_path = $target_dir . basename($_FILES["fileToUpload"]["name"]);
				$uploadOk = 1;
				$imageFileType = strtolower(pathinfo($image_path,PATHINFO_EXTENSION));

				// Check if image file is a actual image or fake image
				$check = getimagesize($file_tmp_name);
				if($check !== false) {
					echo "File is an image - " . $check["mime"] . ".";
					$uploadOk = 1;
					$sql = "UPDATE IGNORE posts SET
							post_title = '$post_title', 
							post_text = '$post_text',
							image_path = '$image_path',
							date_updated = '$date_updated'
							WHERE id='$post_id'";
					if (mysqli_query($conn, $sql)) {
						if(mysqli_affected_rows($conn) == 0){
							echo ' post like that already exists  ';
						}else{
							echo ' post created successfully ';
						}
					} else {
						echo "Error: " . $sql . "<br>" . mysqli_error($conn);
					}
				} else {
					echo "File is not an image.";
					$uploadOk = 0;
				}
				require_once "checks/check_file_upload.php";
			}else{
				// this will update post, if image was not selected
				if($_POST["post_title"] == $post_title_db){
					echo " Post title like that already exists ";
				}else{
					$sql = "UPDATE IGNORE posts SET
						post_title = '$post_title', 
						post_text = '$post_text',
						image_path = '$image_path',
						date_updated = '$date_updated'
						WHERE id='$post_id'";
					if (mysqli_query($conn, $sql)) {
						if(mysqli_affected_rows($conn) == 0){
							echo ' post like that already exists  ';
						}else{
							echo ' post updated successfully ';
						}
					} else {
						echo "Error: " . $sql . "<br>" . mysqli_error($conn);
					}
				}
			}
		}else{
			echo " only admin, or he who made post can edit it ";
		}

		mysqli_close($conn);
	}
}

?>

<br>
<a href="auth/logout.php">Logout</a>


<h3>Create your post here</h3>
<form method="post" action="edit_post.php?id=<?php echo $post_id; ?>"
	  enctype="multipart/form-data">
	Post Title:
	<input type="text" name="post_title" value="<?php echo $post_title_db;?>">
	<span class="error">* <?php echo $post_title_err;?></span>
	<br><br>

	Post Text:
	<textarea name="post_text" rows="5" cols="40"><?php echo $post_text;?></textarea>
	<span class="error">* <?php echo $post_text_err;?></span>
	<br><br>

	<?php echo '<img src='.$image_path.' height=400 width=600 />'; ?><br>
	Select image to upload:
	<input type="file" name="fileToUpload" id="fileToUpload">
	<br><br>

	<input type="submit" name="submit" value="Submit">
	<br><br>
</form>

<br><br>
<a href="index.php">Index</a><br>
<br>
<a href="view_post.php?id=<?php echo $post_id; ?>">View Post</a>

</body>
</html>