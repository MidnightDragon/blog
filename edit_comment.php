<?php
session_start();
require_once 'db_connect.php';

$comment = "";
$comment_err = "";
date_default_timezone_set('Asia/Tbilisi');
$comment_date_created =  date('Y-m-d H:i:s');
$comment_date_updated =  date('Y-m-d H:i:s');

// checks data i recieve from login.php
if(!empty($_SESSION['logged_name'])){
	$user_logged = $_SESSION['logged_name'];
	echo $user_logged;
}else{
	echo " session logged_name is empty ";
}
if(!empty($_SESSION['user_logged_id'])){
	$user_id = $_SESSION['user_logged_id'];
	echo $user_id;
}else{
	echo " session user_logged_id is empty ";
}

$post_id = $_SESSION["post_id"];

if($_SESSION['login'] == true){
	if(isset($_GET['id'])){
		$_SESSION["comment_id"] = (int)$_GET['id'];
		$comment_id = $_SESSION["comment_id"];
		// display comment which i want to edit
		$query = "SELECT 
						id, 
						comment, 
						post_id, 
						user_id, 
						user_name, 
						date_created, 
						date_updated
					FROM comments WHERE id='$comment_id' 
					";
		$result = mysqli_query($conn, $query);
		$comments_list = '';
		while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)){
			$comment_id = $row["id"];
			$comment = $row["comment"];
			$comment_post_id = $row["post_id"];
			$comment_user_id = $row["user_id"];
			$comment_user_name = $row["user_name"];
			$comment_date_created = $row["date_created"];
			$comments_list .= '<p>
				'.$comment.'</br>
				'.$comment_id.'</br>
				'.$comment_post_id.'</br>
				'.$comment_user_id.'</br>
				'.$comment_user_name.'</br>
				
				</p>
				<p>Created by: '.$comment_user_name.'</p>
				Date Created: '.$comment_date_created.'</br>
				<p><a href="edit_comment.php?id='.$comment_id.'">Edit Comment</a></p>
				<hr>';
		}
		if (!$query) {
			die('Invalid query: ' . mysqli_error($conn));
		}

		if($_SERVER["REQUEST_METHOD"] == "POST"){
			require_once "checks/check_create_comment.php";
			if(!$comment_err){
				$query = "SELECT users.id, users.name, posts.user_id 
					FROM users
					JOIN posts ON users.id = posts.user_id
					WHERE posts.id = '$post_id'";
				$result = mysqli_query($conn, $query);
				if (mysqli_query($conn, $query)) {
					//header("Location: index.php");
					$row = mysqli_fetch_array($result, MYSQLI_ASSOC);
					$post_user_name = $row["name"];
					echo " user who created post: ".$post_user_name; echo "</br>";
				} else {
					echo "Error: " . $query . "<br>" . mysqli_error($conn);
				}

				if($user_logged == "sam" || $user_logged == $post_user_name){
					// update comment
					$sql = "UPDATE IGNORE comments SET
				comment = '$comment', 
				date_updated = '$comment_date_updated'
				WHERE id='$comment_id'";
					if (mysqli_query($conn, $sql)) {
						if(mysqli_affected_rows($conn) == 0){
							echo ' post like that already exists  ';
						}else{
							echo ' comment updated successfully ';
						}
					} else {
						echo "Error: " . $sql . "<br>" . mysqli_error($conn);
					}
				}else{
					echo " only admin, or he who made post can edit it ";
				}
			}
		}else{
			//echo " nothing was submitted ";
		}
	}
}else{
	header("Location: auth/login.php");
}
?>

<!DOCTYPE HTML>
<html>
<head>
	<style>
		.error {color: #FF0000;}
		a { text-decoration:; }

		body{ font-family: "Trebuchet MS", Arial, Helvetica, sans-serif; }
		div#pagination_controls{font-size:21px;}
		div#pagination_controls > a{ color:#06F; }
		div#pagination_controls > a:visited{color:#06F;}
	</style>
</head>
<body>
<br>
<a href="index.php">Index</a>

<form method="post" action="edit_comment.php?id=<?php echo $comment_id; ?>">
	Comment:
	<textarea name="comment" rows="5" cols="40"><?php echo $comment;?></textarea>
	<span class="error">* <?php echo $comment_err;?></span>
	<br><br>

	<input type="submit" name="submit" value="Submit">
	<br><br>
</form>

<br><br>
<a href="view_post.php?id=<?php echo $post_id; ?>">View Post</a>

</body>
</html>
