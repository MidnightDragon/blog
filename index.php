<?php
require_once 'db_connect.php';
session_start();

// this checks if a user is logged in
if($_SESSION['login'] == true){
	echo ' user logged: ';
	echo $_SESSION['logged_name'];
}else{
	echo 'nop';
	header("Location: auth/login.php");
}

// checks data i recieve from login.php
if(!empty($_SESSION['logged_name'])){
	$user_logged = $_SESSION['logged_name'];
	//echo " user logged: ".$user_logged;
}else{
	echo " session logged_name is empty ";
}
if(!empty($_SESSION['user_logged_id'])){
	$user_id = $_SESSION['user_logged_id'];
	//echo " id: ".$user_id;
}else{
	echo " session user_logged_id is empty ";
}


$query = "SELECT COUNT(id) AS NumberOfPosts FROM posts";
$result = mysqli_query($conn, $query);
if (!$query) {
	die('Invalid query: ' . mysqli_error($conn));
}
$row = mysqli_fetch_row($result);
// here we have the total row count
$rows = $row[0];
// this is the number of results we want displayed per page
$page_rows = 3; // posts per page
// this tells us the page number of our last page
$last = ceil($rows/$page_rows);
// this makes sure $last cannot be less than 1
if($last < 1){
	$last = 1;
}
// establish the $pagenum variable
$pagenum = 1;
// get pagenum from URL vars if it is present, else set it to 1
if(isset($_GET['pn'])){
	$pagenum = preg_replace('#[^0-9]#', '', $_GET['pn']);
}
// this makes sure that page number isnt below 1, or more than $last page
if($pagenum < 1){
	$pagenum = 1;
}elseif($pagenum > $last){
	$pagenum = $last;
}
// this sets the range of rows to query from the chosen page number
$limit = 'LIMIT '.($pagenum - 1) * $page_rows.','.$page_rows;
// this is for grabbing just one page of rows by applying $limit
$query = "SELECT posts.id, 
				posts.post_title, 
				posts.post_text, 
				posts.user_id, 
				posts.user_name, 
				posts.image_path,
				posts.date_created, 
				posts.date_updated 
		FROM posts
		ORDER BY posts.id DESC $limit";
$result = mysqli_query($conn, $query);
if (!$result) {
	die('Invalid query: ' . mysqli_error($conn));
}
// this shows the user what page they are on, and the total number of pages
$textline1 = "Total posts: (<b>$rows</b>)";
$textline2 = "Page <b>$pagenum</b> of <b>$last</b>";
// establish the $paginationCtrls variable
$paginationCtrls = '';
// if there is more than 1 page worth of results
if($last != 1){
	/*
	first we check if we are on page one. if we are, then we dont need a link to
	the previous page or the first page so we do nothing. if we aren't, then we
	generate links to the first page, and to the previous page.
	*/
	if($pagenum > 1){
		$previous = $pagenum -1;
		$paginationCtrls .= '<a href="'.$_SERVER['PHP_SELF'].'?pn='.$previous.'">
				Previous</a> &nbsp; &nbsp; ';
		// render clickable number links that should appear on the left of target
		// page number
		for($i = $pagenum-4; $i < $pagenum; $i++){
			if($i > 0){
				$paginationCtrls .= '<a href="'.$_SERVER['PHP_SELF'].'?pn='.$i.'">
					'.$i.'</a> &nbsp; ';
			}
		}
	}
	// render the target page number, but without it being a link
	$paginationCtrls .= ''.$pagenum.' &nbsp; ';
	// render the clickable number links that should appear on the right
	for($i = $pagenum+1; $i <= $last; $i++){
		$paginationCtrls .= '<a href="'.$_SERVER['PHP_SELF'].'?pn='.$i.'">
					'.$i.'</a> &nbsp; ';
		if($i >= $pagenum+4){
			break;
		}
	}
	if($pagenum != $last){
		$next = $pagenum + 1;
		$paginationCtrls .= ' &nbsp; &nbsp; 
			<a href="'.$_SERVER['PHP_SELF'].'?pn='.$next.'">Next</a>';
	}
}

$list = '';
while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)){
	$id = $row["id"];
	$image_path = $row["image_path"];
	$post_title = $row["post_title"];
	$post_text = $row["post_text"];
	$post_user_name = $row["user_name"];
	$date_created = $row["date_created"];
	$date_updated = $row["date_updated"];

	// count comments on post here
	$sql = "SELECT COUNT(id) AS NumberOfComments FROM comments
				WHERE post_id='$id'";
	$query = mysqli_query($conn, $sql);
	if (!$query) {
		die('Invalid query: ' . mysqli_error($conn));
	}
	$row = mysqli_fetch_row($query);
	// here we have the total row count
	$total_comments = '';
	$total_comments = $row[0];
	$_SESSION["total_comments"] = $total_comments;


	$list .= '<p><a href="view_post.php?id='.$id.'">'.$post_title.'</a></br>
		'.$id.'</br>
		'.$post_text.'</br>
		<a href="view_post.php?id='.$id.'">
			<img src='.$image_path.' height=200 width=300 />
		</a>
		</p>
		<p>Created by: '.$post_user_name.'</p>
		Amount of comments: '.$total_comments.'</br>
		Date Created: '.$date_created.'</br>
		Date Updated: '.$date_updated.'</br>	
		';
	if($user_logged == "sam" || $user_logged == $post_user_name){
		$list .= '<p><a href="delete_post.php?id='.$id.'">Delete</a></p><hr>';
	}else{
		$list .= '<hr>';
	}
}

mysqli_close($conn);
?>

<!DOCTYPE HTML>
<html>
<head>
	<style>
		.error {color: #FF0000;}
		a { text-decoration:; }

		body{ font-family: "Trebuchet MS", Arial, Helvetica, sans-serif; }
		div#pagination_controls{font-size:21px;}
		div#pagination_controls > a{ color:#06F; }
		div#pagination_controls > a:visited{color:#06F;}
	</style>
</head>
<body>


<br>
<a href="index.php">Index</a>
<br>
<a href="create_post.php">Create Post</a>
<br><br>
<a href="auth/logout.php">Logout</a>
<br>

<div>
	<h2><?php echo $textline1; ?> Paged</h2>
	<p><?php echo $textline2; ?></p>
	<p><?php echo $list; ?></p>

	<div id="pagination_controls"><?php echo $paginationCtrls; ?></div>
</div>


</body>
</html>