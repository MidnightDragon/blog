<!DOCTYPE HTML>
<html>
<head>
	<style>
		.error {color: #FF0000;}
	</style>
</head>
<body>

<?php

// define variables and set to empty values
$nameErr = $passErr = "";
$name = $password =  "";
date_default_timezone_set('Asia/Tbilisi');
$date_created =  date('Y-m-d H:i:s');
$date_updated =  date('Y-m-d H:i:s');
if ($_SERVER["REQUEST_METHOD"] == "POST") {
	require_once 'checks/check_register_form.php';

	if(!$nameErr && !$passErr){
		try{
			require_once 'db_connect.php';

			$sql = "INSERT IGNORE INTO users (name, password, 
							date_created, date_updated)
						VALUES ('$name', '$password', 
								'$date_created', '$date_updated')";
			if (mysqli_query($conn, $sql)) {
				if(mysqli_affected_rows($conn) == 0){
					echo ' user like that already exists  ';
				}else{
					echo ' user created successfully ';
				}
			} else {
				echo "Error: " . $sql . "<br>" . mysqli_error($conn);
			}
			mysqli_close($conn);
		}catch(Exception $e) {
			echo 'Message: ' .$e->getMessage();
		}
	}
}


?>

<h2>Register</h2>
<p><span class="error">* required field.</span></p>
<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
	Name: <input type="text" name="name" value="<?php echo $name;?>">
	<span class="error">* <?php echo $nameErr;?></span>
	<br><br>
	Password: <input type="text" name="password" value="<?php echo $password;?>">
	<span class="error">* <?php echo $passErr;?></span>
	<br><br>
	<input type="submit" name="submit" value="Submit">
</form>

<?php
echo "<h2>Your Input:</h2>";
echo $name;
echo "<br>";
echo $password;
echo "<br>";
?>

<br><br>
<a href="index.php">Index</a><br><br>

</body>
</html>