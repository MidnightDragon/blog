<?php
require_once 'db_connect.php';
session_start();

if($_SESSION['login'] == true){
	echo ' user logged: ';
	echo $_SESSION['logged_name'];

	// checks data i recieve from login.php
	if(!empty($_SESSION['logged_name'])){
		$user_logged = $_SESSION['logged_name'];
		//echo " user logged: ".$user_logged;
	}else{
		echo " session logged_name is empty ";
	}
	if(!empty($_SESSION['user_logged_id'])){
		$user_id = $_SESSION['user_logged_id'];
		//echo " id: ".$user_id;
	}else{
		echo " session user_logged_id is empty ";
	}

	if(isset($_GET['id'])){
		$_SESSION["post_id"] = (int)$_GET['id'];
		$post_id = $_SESSION["post_id"];
		$query = "SELECT 
						id, 
						image_path, 
						post_title, 
						post_text, 
						user_name, 
						date_created, 
						date_updated
					FROM posts WHERE id='$post_id'";
		$result = mysqli_query($conn, $query);
		$list = '';
		while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)){
			$id = $row["id"];
			$image_path = $row["image_path"];
			$post_title = $row["post_title"];
			$post_text = $row["post_text"];
			$post_user_name = $row["user_name"];
			$date_created = $row["date_created"];
			$date_updated = $row["date_updated"];
			$list .= '<p>
				'.$post_title.'</br>
				'.$id.'</br>
				'.$post_text.'</br>
				
				<img src='.$image_path.' height=400 width=600 />
				</p>
				<p>Created by: '.$post_user_name.'</p>
				Date Created: '.$date_created.'</br>
				Date Updated: '.$date_updated.'</br>
				<p><a href="create_comment.php?id='.$id.'">Comment</a></p>		
				';
			if($user_logged == "sam" || $user_logged == $post_user_name){
				$list .= '
					<p><a href="edit_post.php?id='.$id.'">Edit</a></p>
					<p><a href="delete_post.php?id='.$id.'">Delete</a></p><hr>';
			}else{
				$list .= '<hr>';
			}
		}

		// count comments on post here
		$sql = "SELECT COUNT(id) AS NumberOfComments FROM comments
				WHERE post_id='$post_id'";
		$query = mysqli_query($conn, $sql);
		if (!$query) {
			die('Invalid query: ' . mysqli_error($conn));
		}
		$row = mysqli_fetch_row($query);
		// here we have the total row count
		$total_comments = '';
		$total_comments = $row[0];
		//$_SESSION["total_comments"] = $total_comments;

		// display comments here
		$query = "SELECT 
						id, 
						comment, 
						post_id, 
						user_id, 
						user_name, 
						date_created, 
						date_updated
					FROM comments WHERE post_id='$post_id' 
					ORDER BY id DESC";
		$result = mysqli_query($conn, $query);
		$comments_list = '';
		while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)){
			$comment_id = $row["id"];
			$comment = $row["comment"];
			$comment_post_id = $row["post_id"];
			$comment_user_id = $row["user_id"];
			$comment_user_name = $row["user_name"];
			$comment_date_created = $row["date_created"];
			$comment_date_updated = $row["date_updated"];
			$comments_list .= '<p>
				'.$comment.'</br>
				Comment id: '.$comment_id.'</br>
				</p>
				<p>Created by: '.$comment_user_name.'</p>
				Date Created: '.$comment_date_created.'</br>
				Date Updated: '.$comment_date_updated.'</br>
				';
			if($user_logged == "sam" || $user_logged == $comment_user_name){
				$comments_list .= '
					<p><a href="edit_comment.php?id='.$comment_id.'">Edit Comment</a></p>
				<p><a href="delete_comment.php?id='.$comment_id.'">Delete Comment</a></p>
				<hr>';
			}else{
				$comments_list .= '<hr>';
			}
		}
		
		if (!$query) {
			die('Invalid query: ' . mysqli_error($conn));
		}

	}
}else{
	header("Location: auth/login.php");
}
?>

<!DOCTYPE HTML>
<html>
<head>
	<style>
		.error {color: #FF0000;}
		a { text-decoration:; }

		body{ font-family: "Trebuchet MS", Arial, Helvetica, sans-serif; }
		div#pagination_controls{font-size:21px;}
		div#pagination_controls > a{ color:#06F; }
		div#pagination_controls > a:visited{color:#06F;}
		</style>
</head>
<body>
<br>
<a href="index.php">Index</a>
<p><?php echo $list; ?></p>
<h1>Comments: <?php echo $total_comments; ?></h1><hr>
<p><?php echo $comments_list; ?></p>


</body>
</html>
