-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 24, 2017 at 04:07 PM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `blog`
--

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `comment` varchar(255) NOT NULL,
  `post_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `comment`, `post_id`, `user_id`, `user_name`, `date_created`, `date_updated`) VALUES
(62, 'ahahahahh wsup here', 127, 78, 'john', '2017-12-18 11:21:06', '2017-12-19 14:30:28'),
(95, 'wharf', 141, 77, 'sam', '2017-12-23 18:38:54', '2017-12-23 18:38:54'),
(96, 'scarf', 141, 77, 'sam', '2017-12-23 18:38:57', '2017-12-23 18:38:57'),
(97, 'burpy', 141, 77, 'sam', '2017-12-23 18:39:01', '2017-12-23 18:39:01'),
(102, 'sway king hemiron', 157, 77, 'sam', '2017-12-23 18:50:02', '2017-12-23 18:50:25'),
(103, 'blum', 157, 77, 'sam', '2017-12-23 18:50:05', '2017-12-23 18:50:05'),
(104, 'pff raka', 157, 77, 'sam', '2017-12-23 18:50:06', '2017-12-23 18:50:15'),
(105, 'stephen', 158, 78, 'john', '2017-12-23 20:07:19', '2017-12-23 20:07:19'),
(106, 'king', 158, 78, 'john', '2017-12-23 20:07:24', '2017-12-23 20:07:24'),
(107, 'jump', 158, 78, 'john', '2017-12-23 20:07:28', '2017-12-23 20:07:28'),
(108, 'wrath', 158, 77, 'sam', '2017-12-23 20:07:51', '2017-12-23 20:07:51'),
(115, 'ასდადაად', 158, 77, 'sam', '2017-12-23 21:42:50', '2017-12-23 21:42:50'),
(116, 'აბრიკოს ი პერსიკ\r\n\r\nშლი პო დოროგე', 158, 77, 'sam', '2017-12-23 21:43:19', '2017-12-23 21:43:19');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(11) NOT NULL,
  `post_title` varchar(255) NOT NULL,
  `post_text` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `image_path` varchar(255) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `post_title`, `post_text`, `user_id`, `user_name`, `image_path`, `date_created`, `date_updated`) VALUES
(127, 'asdlele', 'zxcvxcv', 77, 'sam', 'uploads/2f836c3dc8e4031ccc7d36b5d4bed41b.jpg', '2017-12-18 11:21:06', '2017-12-23 14:45:23'),
(128, 'post 2', 'text 2', 77, 'sam', 'uploads/_comission__bg_srifi_no_1_by_maxmail25196-d9f64n4.jpg', '2017-12-18 11:21:39', '2017-12-18 11:21:39'),
(129, 'post 3', 'text 3', 77, 'sam', 'uploads/1bZ1FWP.jpg', '2017-12-18 11:21:50', '2017-12-18 11:21:50'),
(130, 'post 4', 'text 4', 77, 'sam', 'uploads/1-Q4nI4OLyWQu6zWQZbK14EA.png', '2017-12-18 11:22:01', '2017-12-18 11:22:01'),
(133, 'post 5', 'text 5', 77, 'sam', 'uploads/2f836c3dc8e4031ccc7d36b5d4bed41b.jpg', '2017-12-18 12:11:56', '2017-12-18 12:11:56'),
(134, 'post 6', 'text 6', 77, 'sam', 'uploads/2RcVrwm.jpg', '2017-12-18 12:12:10', '2017-12-18 12:12:10'),
(135, 'post 7', 'text 7', 77, 'sam', 'uploads/3a1234d100723788422695ca19f2bd62.jpg', '2017-12-18 12:12:19', '2017-12-18 12:12:19'),
(137, 'post 8', 'text 8', 77, 'sam', 'uploads/5-centimeters-per-second-15139.jpg', '2017-12-18 12:13:17', '2017-12-18 12:13:17'),
(138, 'while loop', 'is working fine', 77, 'sam', 'uploads/60kilometers_per_hour_by_mclelun-d8lmndc.jpg', '2017-12-18 12:18:49', '2017-12-19 15:11:14'),
(141, 'whatever it is', 'its gone lel', 77, 'sam', 'uploads/____by_baxiaart-daqbqd2.jpg', '2017-12-18 12:47:02', '2017-12-23 18:17:41'),
(157, 'rastafarian', 'religion', 77, 'sam', 'uploads/_comission__bg_srifi_no_1_by_maxmail25196-d9f64n4.jpg', '2017-12-23 18:41:57', '2017-12-23 18:41:57'),
(158, 'wraith', 'coolers', 78, 'john', 'uploads/3629b_by_pascalcampion-d9hy798.jpg', '2017-12-23 20:07:08', '2017-12-23 20:07:08');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `password`, `date_created`, `date_updated`) VALUES
(77, 'sam', '123', '2017-12-13 00:00:00', '2017-12-13 00:00:00'),
(78, 'john', '123', '2017-12-13 00:00:00', '2017-12-13 00:00:00'),
(81, 'samae', '1234', '2017-12-13 00:00:00', '2017-12-13 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `post_title` (`post_title`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=117;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=159;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
