<?php
// checking name from post
if (empty($_POST["name"])) {
	$nameErr = "Name is required";
} else {
	$_SESSION["name"] = test_input($_POST["name"]);
	$name = $_SESSION["name"];
	// check if name only contains letters and whitespace
	if (!preg_match("/^[a-zA-Z ]*$/",$name)) {
		$nameErr = "Only letters and white space allowed";
	}
}

// checking password from post
if (empty($_POST["password"])) {
	$passErr = "Password is required";
} else {
	$_SESSION["password"] = test_input($_POST["password"]);
	$password = $_SESSION["password"];
}

