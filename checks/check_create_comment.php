<?php
if(empty($_POST['comment'])){
	$comment_err = ' comment is required ';
}else{
	$_SESSION['comment'] = test_input($_POST['comment']);
	$comment = $_SESSION["comment"];

	// check if name only contains letters and whitespace
	if (!preg_match("/^[\r\n0-9a-zA-Zა-ჰ \/_:,.?@;-]+$/",$comment)) {
		$comment_err = "Only letters, numbers and white space allowed";
	}
}

function test_input($data){
	//$data = trim($data);
	$data = stripslashes($data);
	$data = htmlspecialchars($data);
	return $data;
}
