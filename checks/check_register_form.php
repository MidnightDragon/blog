<?php
if (empty($_POST["name"])) {
	$nameErr = "Name is required";
} else {
	$name = test_input($_POST["name"]);
	// check if name only contains letters and whitespace
	if (!preg_match("/^[a-zA-Z ]*$/",$name)) {
		$nameErr = "Only letters and white space allowed";
	}
}

if (empty($_POST["password"])) {
	$passErr = "Password is required";
} else {
	$password = test_input($_POST["password"]);
}

function test_input($data) {
	$data = trim($data);
	$data = stripslashes($data);
	$data = htmlspecialchars($data);
	return $data;
}

