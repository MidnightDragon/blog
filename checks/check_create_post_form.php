<?php
if(empty($_POST['post_title'])){
	$post_title_err = 'post title is required';
}else{
	$_SESSION['post_title'] = test_input($_POST['post_title']);
	$post_title = $_SESSION["post_title"];

	// check if name only contains letters and whitespace
	if (!preg_match("/^[a-zA-Z ა-ჰ 0-9]*$/",$post_title)) {
		$post_title_err = "Only letters, numbers and white space allowed";
	}
}

if (empty($_POST["post_text"])) {
	$post_text_err = "Post text is required";
} else {
	$_SESSION["post_text"] = test_input($_POST["post_text"]);
	$post_text = $_SESSION["post_text"];
}


function test_input($data){
	$data = trim($data);
	$data = stripslashes($data);
	$data = htmlspecialchars($data);
	return $data;
}

