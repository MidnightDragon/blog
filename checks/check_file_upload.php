<?php
// Check if file already exists
if (file_exists($image_path)) {
	echo "Sorry, file already exists.";
	$uploadOk = 0;
}
// Check file size
if ($file_size > 5500000) {
	echo "Sorry, your file is too large.";
	$uploadOk = 0;
}
// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
	&& $imageFileType != "gif" ) {
	echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
	$uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
	echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
} else {
	if (move_uploaded_file($file_tmp_name, $image_path)) {
		echo "The file ". basename( $file_name). " has been uploaded.";
	} else {
		echo "Sorry, there was an error uploading your file.";
	}
}